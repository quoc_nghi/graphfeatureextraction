import os
import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
import numpy as np
from networkx.drawing import nx_agraph
from networkx.linalg import graphmatrix
import util
import errno

# with open("../sample/100/1579.bc.dot", "r") as ins:
# 	data=ins.read()
url = "/home/quocnghi/courses/software_mining/extraction/"
# url = "/Users/quocnghi/python/graphfeatureextraction/"
for root,dirs,files in os.walk(url + "DotData"):
	for file in files:
		path = os.path.join(root,file)

		# print path
		# print path.split("/")[7]
		with open(path, "r") as ins:
			data=ins.read()
		try:
			G=nx_agraph.from_agraph(pygraphviz.AGraph(data))
			adjacency_matrix = graphmatrix.adjacency_matrix(G)
			# print adjacency_matrix.toarray()
			# print file
			adjacency_directory = url + "AdjacencyData/" + path.split("/")[7]
			print adjacency_directory
			try:
				os.makedirs(adjacency_directory)
			except OSError as exception:
				np.savetxt(adjacency_directory + "/" + file.split(".")[0] + ".txt",adjacency_matrix.toarray().astype(int),fmt="%i")
			np.savetxt(adjacency_directory + "/" + file.split(".")[0] + ".txt",adjacency_matrix.toarray().astype(int),fmt="%i")
		except:
			continue