import pandas as pd
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import StratifiedKFold
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import precision_score
import numpy as np


def classify(X, y, fold, algo):
    skf = StratifiedKFold(n_splits=fold)
    precisions = []
    for train_index, test_index in skf.split(X, y):
        print train_index
        print test_index
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        if algo == 'linearsvm':
            model = OneVsRestClassifier(LinearSVC(random_state=0)).fit(X_train, y_train)
        if algo == 'NB':
            print X_train.shape, y_train.shape
            model = MultinomialNB().fit(X_train, y_train)
        if algo == 'LR':
            model = OneVsRestClassifier(LogisticRegression(C=1e5)).fit(X_train, y_train)
        if algo == 'DT':
            model = OneVsRestClassifier(tree.DecisionTreeClassifier()).fit(X_train, y_train)
        if algo == 'RF':
            model = OneVsRestClassifier(RandomForestClassifier()).fit(X_train, y_train)

        y_pred = model.predict(X_test)
        # print y_test
        # print y_pred
        precisions.append(precision_score(y_test, y_pred, average='micro'))

    precisions = np.asarray(precisions)
    print 'Precision: %f of algorithm %s' % (np.average(precisions), algo)


if __name__ == '__main__':
    fold = 10
    df = pd.read_csv('./features.txt', header=None)
    # print df.shape
    features = list(df.columns[:523])
    X = df[features]
    sel = VarianceThreshold(threshold=.8 * (1 - .8))
    X = sel.fit_transform(X)
    # print X.shape
    y = df[523]
    # print y.shape

    # algorithms = ['DT', 'NB', 'LR',  'linearsvm']
    algorithms = ['RF']
    for algo in algorithms:
        # print train_index
        # print test_index
        classify(X, y, fold, algo)

# y_true = [0, 1, 2, 0, 1, 2]# print train_index
# print test_index
# y_pred = [0, 2, 1, 0, 0, 1]
# print precision_score(y_true, y_pred, average='macro')
# print type(precision_score(y_true, y_pred, average='macro'))
