import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import StratifiedKFold
import random
from sklearn.preprocessing import LabelBinarizer
from sklearn import preprocessing
from sklearn.metrics import precision_score
import sys

def neural_network_model(data, num_layers):
    print "Using " + device_name + " for layers"
    with tf.device(device_name):
        hidden_1_layer = {'weights': tf.Variable(tf.random_normal([cols, n_nodes_hl1])),
                          'biases': tf.Variable(tf.random_normal([n_nodes_hl1]))}

        hidden_2_layer = {'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])),
                          'biases': tf.Variable(tf.random_normal([n_nodes_hl2]))}

        # hidden_3_layer = {'weights': tf.Variable(tf.random_normal([n_nodes_hl2, n_nodes_hl3])),
        #                   'biases': tf.Variable(tf.random_normal([n_nodes_hl3]))}

        output_layer = {'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_classes])),
                        'biases': tf.Variable(tf.random_normal([n_classes]))}

        
        previous_l = tf.add(tf.matmul(data, hidden_1_layer['weights']), hidden_1_layer['biases'])
        previous_l = tf.nn.relu(previous_l)
        # (input_data * weights) + biases
       
        for i in range(num_layers - 1):
            current_l = tf.add(tf.matmul(previous_l, hidden_2_layer['weights']), hidden_2_layer['biases'])
            current_l = tf.nn.relu(current_l)
            previous_l = current_l
      
            


        # l1 = tf.add(tf.matmul(data, hidden_1_layer['weights']), hidden_1_layer['biases'])
        # l1 = tf.nn.dropout(l1, 0.8)
        # l1 = tf.nn.relu(l1)

        # l2 = tf.add(tf.matmul(l1, hidden_2_layer['weights']), hidden_2_layer['biases'])
        # l2 = tf.nn.relu(l2)

        # l3 = tf.add(tf.matmul(l2, hidden_3_layer['weights']), hidden_3_layer['biases'])
        # l3 = tf.nn.relu(l3)




        # output = tf.add(tf.matmul(l2, output_layer['weights']), output_layer['biases'])
        output = tf.add(tf.matmul(previous_l, output_layer['weights']), output_layer['biases'])

    return output


def train_neural_network(x, y, keep_prob, train_x, train_y, test_x, test_y,num_layers):
    prediction = neural_network_model(x, num_layers)

    with tf.device(device_name):
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels= y))

        # learning_rate = 0.001
        optimizer = tf.train.AdamOptimizer().minimize(cost)

    # feedforward and backward
    

    # rows_train, cols_train = train_x.shape
    config = tf.ConfigProto(allow_soft_placement=True)

    with tf.Session(config=config) as sess:
        sess.run(tf.initialize_all_variables())
        # sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()
        for epoch in range(hm_epochs):
            epoch_loss = 0
            # print rows_train / batch_size
            # exit()
            # fold_batch_size = StratifiedKFold(n_splits=rows_train/batch_size)
            # for train_fold_index, test_fold_index in fold_batch_size.split(train_x, train_y):
            #     batch_x = np.array(train_x[train_fold_index])
            #     batch_y = np.array(train_y[train_fold_index])
            #     _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
            #     epoch_loss += c

            i = 0
            while i < len(train_x):
                start = i
                end = i + batch_size
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])
                _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y, keep_prob: 0.8})
                epoch_loss += c
                i += batch_size

            print 'Epoch', epoch, 'completed out of', hm_epochs, 'loss:', epoch_loss

        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

        y_pred = prediction.eval({x: test_x})
        # print np.argmax(y_pred, 1).shape
        # print np.argmax(test_y, 1).shape

        precScore = precision_score(np.argmax(test_y, 1), np.argmax(y_pred, 1), average='micro')
        print 'Accuracy:', accuracy.eval({x: test_x, y: test_y})
        print 'Precision: %f' % precScore

        save_path = saver.save(sess, model_path)
        print "Model saved in file: %s" % save_path
    return precScore


if __name__ == '__main__':

    device_name = sys.argv[1]
    local = sys.argv[2]
    batch_size = int(sys.argv[3])
    hm_epochs = int(sys.argv[4])
    num_layers = int(sys.argv[5])

    print "Local : " + local
    print "Batch size : " + str(batch_size)
    print "Epochs : " + str(hm_epochs)
    print "Number of layers : " + str(num_layers)
    if device_name == "gpu":
        print "Using GPU --------------------------"
        device_name = "/gpu:0"
    else:
        print "Using CPU --------------------------"
        device_name = "/cpu:0"

    if local == "local":
        model_path = "../features/model.ckpt"
        df = pd.read_csv('../features/all_handcrafted_features.csv')
    else:
        model_path = "model.ckpt"
        df = pd.read_csv('all_handcrafted_features.csv')

    # print df.shape
    features = list(df.columns[1:646])
    print "features"
    X_ = df[features]
    # sel = VarianceThreshold(threshold=.1 * (1 - .8))
    # X_ = sel.fit_transform(X_)
    X_ = preprocessing.normalize(X_)
    rows, cols = X_.shape
    print "X_.shape"

    lb = LabelBinarizer()
    y_ = lb.fit_transform(np.array(df["label"]))
    
    # print y_
    # print X_.shape, y_.shape
    # exit()

    n_nodes_hl1 = 300
    n_nodes_hl2 = 300
    # n_nodes_hl3 = 50

    # hm_epochs = 500
    n_classes = 104
    # batch_size = 100

    fold = 10
    skf = StratifiedKFold(n_splits=fold,random_state=1)
    precision = []

    with tf.device(device_name):
       
        keep_prob = tf.placeholder(tf.float32)
       

        #  height x width
        x = tf.placeholder(tf.float32, [None, cols])  # 34
        y = tf.placeholder(tf.float32)

    for train_index, test_index in skf.split(X_, df["label"]):
        X_train, X_test = X_[train_index], X_[test_index]
        y_train, y_test = y_[train_index], y_[test_index]

        print "y test #############"
        print "y_test"
        rows_train, cols_train = X_train.shape
        random_number = random.sample(range(rows_train), rows_train)
        X_train = X_train[random_number]
        y_train = y_train[random_number]

        prec = train_neural_network(x, y, keep_prob, X_train, y_train, X_test, y_test,num_layers)
        precision.append(prec)
    precision = np.array(precision)
    print "Precision of all folds: %f", np.average(precision)
