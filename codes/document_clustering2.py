import openpyxl
import numpy as np
import pandas as pd
import nltk
from bs4 import BeautifulSoup
import re
import os
import codecs
from sklearn import feature_extraction
import mpld3
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import DBSCAN
from nltk.stem.snowball import SnowballStemmer
from scipy.spatial.distance import cdist, pdist
import matplotlib.pyplot as plt
import seaborn
from sklearn.cluster import MeanShift, estimate_bandwidth

stemmer = SnowballStemmer("english")

def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


all_desc = []

for root,dirs,files in os.walk("/home/quocnghi/courses/software_mining/extraction/DocumentData"):
  for file in files:
    path = os.path.join(root,file)
    with open(path, "r") as ins:
        data=ins.readlines()
    buff = "".join(data)
    all_desc.append(buff)

print "Num doc : "  + str(len(all_desc))
tfidf_vectorizer = TfidfVectorizer(max_df=0.8, max_features=200000,
                                 min_df=0.2,decode_error='ignore',
                                 use_idf=True, ngram_range=(1,3))

tfidf_matrix = tfidf_vectorizer.fit_transform(all_desc)

bandwidth = estimate_bandwidth(tfidf_matrix, quantile=0.2, n_samples=20)
ms = MeanShift(bandwidth=bandwidth,bin_seeding=True)
ms.fit(tfidf_matrix)
labels = ms.labels_
print labels
cluster_centers = ms.cluster_centers_
labels_unique = np.unique(labels)
n_clusters_ = len(labels_unique)
print "NUMMMMMMM : " + str(n_clusters_)
# n= [2,3,4,5,6,7,8,9,10,11,12,13,14,15]
# kMeansVar = [KMeans(n_clusters=k).fit(tfidf_matrix) for k in range(2, 16)]
# centroids = [X.cluster_centers_ for X in kMeansVar]
# k_euclid = [cdist(tfidf_matrix, cent) for cent in centroids]
# dist = [np.min(ke, axis=1) for ke in k_euclid]
# wcss = [sum(d**2) for d in dist]
# tss = sum(pdist(tfidf_matrix)**2)/tfidf_matrix.shape[0]
# bss = tss - wcss
# plt.plot(bss)
# plt.show() 


# print(tfidf_matrix.shape)

# num_clusters = 10

# km = KMeans(n_clusters=num_clusters)

# km.fit(tfidf_matrix)

# clusters = km.labels_.tolist()

# print km.labels_


# db = DBSCAN(eps=0.5, min_samples=10).fit(tfidf_matrix)

# print db.labels_

# count = 1
# for ele in db.labels_:
#   if ele == 0:
#     print count
#   count+=1
# num_clusters = 5

# km = KMeans(n_clusters=num_clusters)

# %time km.fit(tfidf_matrix)

# clusters = km.labels_.tolist()