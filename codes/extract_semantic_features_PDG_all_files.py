import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
from networkx.drawing import nx_agraph
import os
from subprocess import call
from extract_dominants import extract_patterns
from extract_dominants import extract_patterns2
import copy
import pydot
import csv
from extract_dominants import get_sorted_nodes_dictionary
import operator

def extract_function_call_nodes(G):
	function_call_dict = {}
	for n,d in G.nodes_iter(data=True):
		if "label" in d:
			if "ii" in d["label"] and "_" in d["label"]:
				label = d["label"].replace("{","").replace("}","")
				function_call_dict[n] = label
	return function_call_dict

def extract_main_node(G):
	for n,d in G.nodes_iter(data=True):
		if "label" in d:
			if "main" in d["label"]:
				return n
	return None




DIC_URL = "/home/quocnghi/courses/software_mining/extraction/features/ir_dictionary.csv"

ir_dict = {}
with open(DIC_URL, mode='r') as infile:
	reader = csv.reader(infile)
	for rows in reader:
		k = rows[0]
		v = rows[1]
		ir_dict[k] = v


ROOT_URL = "/home/quocnghi/courses/software_mining/extraction/";
# ROOT_URL = "/Users/quocnghi/python/graphfeatureextraction/"

source = os.listdir(os.path.join(ROOT_URL,"CFGCallGraphData"))

for dir in source:
	print dir
	dir_path = os.path.join(ROOT_URL,"CFGCallGraphData",dir)
	
	for root,dirs,files in os.walk(dir_path):
		for subdir in dirs:
			index = dir + "_" + subdir
			subdir_path = os.path.join(root,subdir)
			# print subdir_path

			CALLGRAPH_PATH = os.path.join(subdir_path,"callgraph.dot")
			with open(CALLGRAPH_PATH, "r") as ins:
				callgraph_data=ins.read()
			G=nx_agraph.from_agraph(pygraphviz.AGraph(callgraph_data))

			main_node = extract_main_node(G)
			if main_node != None:
				function_call_nodes = extract_function_call_nodes(G)

				print function_call_nodes
				calls_from_main = []
				for edge in list(nx.bfs_edges(G,main_node)):
					if edge[0] == main_node:
						if edge[1] in function_call_nodes:
							calls_from_main.append(edge[1])


				print "calls_from_main : " + str(calls_from_main)
				second_layer_calls = []
				for call in calls_from_main:
					for edge in list(nx.bfs_edges(G,call)):
						if edge[0] == call:
							if edge[1] in function_call_nodes:
								second_layer_calls.append(edge[1])

				print "second_layer_calls : " + str(second_layer_calls)  

				node_order_calls = list()
				node_order_calls.extend(calls_from_main)
				node_order_calls.extend(second_layer_calls)

				function_order_calls = []
				for node in node_order_calls:
					function_order_calls.append(function_call_nodes[node])
				# for node in node_order_calls:

				function_order_calls = ["main"] + function_order_calls
				print function_order_calls
				write_array = []
				for function in function_order_calls:
					function_cfg_path = os.path.join(subdir_path,"cfg." + function + ".dot")
					
					# pydot_graphs = pydot.graph_from_dot_file(function_cfg_path)
					with open(function_cfg_path, "r") as ins:
						data=ins.read()

					G=nx_agraph.from_agraph(pygraphviz.AGraph(data))
					# G=nx_agraph.from_agraph(pygraphviz.AGraph(data))
					# G = nx_pydot.from_pydot(pydot_graphs[0])
					sorted_graph = get_sorted_nodes_dictionary(G)
					index_array = list()
					for n,d in sorted_graph.items():
						print "#########"
						if "label" in d:
							ir_lines = d["label"].split("\l")
							# print d["label"]
							# print ir_lines
							for ir_line in ir_lines:
								# print ir_line
								ir_tokens = ir_line.split(" ")
								# print ir_tokens
								for token in ir_tokens:
									
									if token in ir_dict:
										# print token
										index_array.append(ir_dict[token])
									if "[" in token and "call" not in token:
										index_array.append(ir_dict["array"])
					write_array = write_array + index_array


				print write_array
				line = index + "," + ",".join(write_array)
				with open("../features/semantics_machine_code_indexes2.txt", "a") as f:
				
					print line
					f.write(line + "\n")