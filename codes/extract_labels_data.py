import pandas as pd
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import svm
from sklearn import linear_model
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import VarianceThreshold
from sklearn.multiclass import OneVsOneClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn import linear_model
from sklearn.externals import joblib
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from utils import plot_confusion_matrix
from sklearn.metrics import classification_report
from utils import plot_classification_report
import numpy as np
import matplotlib.pyplot as plt
import xgboost as xgb

# df = pd.read_csv("deep_features1234567.txt", header = None)
# df = pd.read_csv("final_features.txt", header = None)

FIGURE = "../figure/"

# df = pd.read_csv("../features/final_features_with_sample_id_full.txt")
df = pd.read_csv("../features/sample_joins4.csv")

extract_20_labels_df = df.loc[df["label"].isin([27,40,69,67,26,7,65,5,49,31,43,19,104,53,13,38,86,10,98,17])]

# extract_20_labels_df.to_csv("../features/features_20_classes_with_id_1.csv",sep=",",encoding="utf-8",index=False)
extract_20_labels_df.to_csv("../features/features_20_classes_new.csv",sep=",",encoding="utf-8",index=False)