import openpyxl
import numpy as np
import pandas as pd
import nltk
from bs4 import BeautifulSoup
import re
import os
import codecs
from sklearn import feature_extraction
import mpld3
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import DBSCAN
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")

def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems

wb = openpyxl.load_workbook('../descriptions.xlsx')

worksheet = wb.get_sheet_by_name('problemlist')
all_desc = list()

for row in worksheet.iter_rows('D2:D105'.format(worksheet.min_row,worksheet.max_row)):
    for cell in row:
      if cell.value:
        
        all_desc.append(cell.value)

# print all_desc

tfidf_vectorizer = TfidfVectorizer(max_df=0.8, max_features=200000,
                                 min_df=0.2, stop_words='english',
                                 use_idf=True, tokenizer=tokenize_and_stem, ngram_range=(1,3))

tfidf_matrix = tfidf_vectorizer.fit_transform(all_desc)

print(tfidf_matrix.shape)

num_clusters = 5

km = KMeans(n_clusters=num_clusters)

km.fit(tfidf_matrix)

clusters = km.labels_.tolist()

print km.labels_


db = DBSCAN(eps=0.5, min_samples=10).fit(tfidf_matrix)

print db.labels_

count = 1
for ele in db.labels_:
  if ele == 0:
    print count
  count+=1
# num_clusters = 5

# km = KMeans(n_clusters=num_clusters)

# %time km.fit(tfidf_matrix)

# clusters = km.labels_.tolist()