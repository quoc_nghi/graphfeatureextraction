import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
# import xlsd
from networkx.drawing import nx_agraph



with open("../sample/1/40.bc.dot", "r") as ins:
	data=ins.read()

with open("../sample/1/17.bc.dot", "r") as ins:
	data2=ins.read()

G1=nx_agraph.from_agraph(pygraphviz.AGraph(data))
# G2=nx.convert_node_labels_to_integers(G)
G2=nx_agraph.from_agraph(pygraphviz.AGraph(data2))

print nx.is_isomorphic(G1, G2)

print nx.fast_could_be_isomorphic(G1,G2)

print nx.could_be_isomorphic(G1,G2)