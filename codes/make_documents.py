import os
import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from networkx.drawing import nx_agraph
import util

document_url = "/home/quocnghi/courses/software_mining/extraction/DocumentData"

for root,dirs,files in os.walk("/home/quocnghi/courses/software_mining/extraction/ProgramData"):
	for dir in dirs:
		document = ""
		dir_path = os.path.join(root,dir)
		index = 0
		print "------------------------------"
		for filename in os.listdir(dir_path):
			path = os.path.join(dir_path,filename)
			
			print path.split("/")[7]
			with open(path, "r") as ins:
				data=ins.read()
			
			data = data.replace("}","").replace("{","").replace("()","").replace("(","").replace(")","")
			data = data.replace("\n"," ").replace("\r","").replace("#","").replace(";","")
			data = data.replace("include <iomanip>","").replace("include <string.h>","").replace("include <wchar.h>","").replace("include <math.h>","").replace("include <stdio.h>","").replace("include <iostream>","").replace("include <math.h>","using namespace std")
			print data
			document += data
			documment = "".join(document) 
			new = document_url + "/" + path.split("/")[7] + ".txt"
			with open(new, "a") as f:
				f.write(document + "\n")