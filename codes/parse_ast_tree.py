from __future__ import print_function
import argparse
import sys
from pycparser import c_parser, c_ast, parse_file
import re
def stripcomments(line):
	return re.sub('//.*?\n|/\*.*?\*/',line,flags=re.S)

def removeComments(string):
    string = re.sub(re.compile("/\*.*?\*/",re.DOTALL ) ,"" ,string) # remove all occurance streamed comments (/*COMMENT */) from string
    string = re.sub(re.compile("//.*?\n" ) ,"" ,string) # remove all occurance singleline comments (//COMMENT\n ) from string
    return string

PROGRAM_PATH = "/home/quocnghi/courses/software_mining/extraction/sample_cpp/13.cpp";

with open(PROGRAM_PATH, "r") as ins:
	data =ins.read()

parser = c_parser.CParser()
data = removeComments(data)

ast = parser.parse(data, filename='<none>')

ast.show()
function_decl = ast.ext[0].body
print(function_decl.block_items[1].name)
print(ast.ext[0].decl.name)
print(function_decl.block_items[0].name)

print("----------------------------------")
for ext in ast.ext:
	print("####################")
	ext_decl_name = ext.decl.name
	print(ext_decl_name)
	function_decl = ext.body
	variables_decl = function_decl.block_items
	print("****")
	for variable in variables_decl:
		# print(str(type(variable)))
		if "Decl" in str(type(variable)):
			print(variable.name)
		if "FuncCall" in str(type(variable)):
			print(variable.name.name)
			print(variable.args)
		
	


# if __name__ == "__main__":
#     argparser = argparse.ArgumentParser('Dump AST')
#     argparser.add_argument('filename', help='name of file to parse')
#     args = argparser.parse_args()

#     ast = parse_file(args.filename, use_cpp=False)
#     print(ast)
#     # ast = stripcomments(ast)
#     # ast.show()
#     function_decl = ast.ext[0].body
#     print(function_decl.children)
