import pandas as pd
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import svm
from sklearn import linear_model
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import VarianceThreshold
from sklearn.multiclass import OneVsOneClassifier
from sklearn.multiclass import OneVsRestClassifier

from sklearn.externals import joblib
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier


df = pd.read_csv("binary.txt", header = None)

features = list(df.columns[:523])

y = df[523]

X = df[features]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=0)


sel = VarianceThreshold(threshold=(.8*(1-.8)))
X = sel.fit_transform(X)


clf = svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
# print "Score with SVM linear : " + str(clf.score(X_test, y_test)) 

y_pred  = clf.predict(X_test);
print "Precision 2 fold for SVM linear : " + str(precision_score(y_pred, y_test,average = "micro"))
print "Recall 2 fold for SVM linear : " + str(recall_score(y_pred, y_test,average = "micro"))
print "F1 2 fold for SVM linear :" + str(f1_score(y_pred, y_test, average='micro')  )


print "#------------------------------------------"
#------------------------------------------------------------------------------------
clf2 = DecisionTreeClassifier(min_samples_split=20, random_state=99).fit(X_train, y_train)

y_pred2  = clf2.predict(X_test);
print "Precision 2 fold for DecisionTree : " + str(precision_score(y_pred2, y_test,average = "micro"))
print "Recall 2 fold for DecisionTree : " + str(recall_score(y_pred2, y_test,average = "micro"))
print "F1 2 fold for with DecisionTree :" + str(f1_score(y_pred2, y_test, average='micro')  )

print "#------------------------------------------"

#------------------------------------------------------------------------------------
clf3 = GaussianNB().fit(X_train,y_train)
y_pred3  = clf3.predict(X_test);
print "Precision 2 fold for Naive Bayes : " + str(precision_score(y_pred3, y_test,average = "micro"))
print "Recall 2 fold for Naive Bayes : " + str(recall_score(y_pred3, y_test,average = "micro"))
print "F1 2 fold for Naive Bayes :" + str(f1_score(y_pred3, y_test, average='micro')  )


print "#------------------------------------------"

#------------------------------------------------------------------------------------
clf4 = Perceptron(n_iter=50).fit(X_train,y_train)
y_pred4  = clf4.predict(X_test);
print "Precision 2 fold for Perceptron : " + str(precision_score(y_pred4, y_test,average = "micro"))
print "Recall 2 fold for Perceptron : " + str(recall_score(y_pred4, y_test,average = "micro"))
print "F1 2 fold for Perceptron :" + str(f1_score(y_pred4, y_test, average='micro')  )
