import os
import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
# import xlsd
from networkx.drawing import nx_agraph
# import util
import pydot


PDG_PATH = "/home/quocnghi/courses/software_mining/extraction/DotData/1/1001.bc.dot" 


with open(PDG_PATH, "r") as ins:
	data=ins.read()
print data
# G=nx_agraph.from_agraph(pygraphviz.AGraph(data))

pydot_graphs = pydot.graph_from_dot_file(PDG_PATH)

G = nx_pydot.from_pydot(pydot_graphs[1])
# print G[0].get_node_list()
print "num nodes : " + str(len(G.nodes()))

# nx.draw(G,with_labels=True)
# plt.show()