import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
# import xlsd
from networkx.drawing import nx_agraph

def get_pq_nodes_dictionary(intersection, out_dict, in_dict):
	pq_nodes_dict = {}
	for key in intersection:
		if (key in out_dict) and (key in in_dict):
			pq_key = "(" + str(in_dict[key]) + "," + str(out_dict[key]) + ")"
			
			# print "Node :" + str(key) + " is a " + "(" + str(in_dict[key]) + "," + str(out_dict[key]) + ")"
		else:
			if key in out_dict:
				pq_key = "(" + "0" + "," + str(out_dict[key]) + ")"
				# print "Node :" + str(key) + " is a " + "(" + "0" + "," + str(out_dict[key]) + ")"
			if key in in_dict:
				pq_key = "(" + str(in_dict[key]) + "," + "0" + ")"	
				# print "Node :" + str(key) + " is a " + "(" + str(in_dict[key]) + "," + "0" + ")"	
		if pq_key in pq_nodes_dict:
			pq_nodes_dict[pq_key] +=1
		else:
			pq_nodes_dict[pq_key] = 1
	return pq_nodes_dict

def get_degree_dictionary(graph):
	nodes = list(graph.nodes())

	return graph.degree(nodes)

def get_degree_features_dictionary(degree_dictionary):
	counter = Counter(degree_dictionary.values())
	dictionary = dict(counter)
	new_dict = {}
	for item in dictionary:
		name = str(item) + "-degree"
		print name
		new_dict[name] = dictionary[item]
	return new_dict

def get_in_nodes_dictionary(G):
	in_dict = {}
	nodes = list(G.nodes())
	in_edges = G.in_edges()
	for node in nodes:
		for in_edge in in_edges:
			if in_edge[1] == node:
				if node in in_dict:
					in_dict[node] +=1
				else:
					in_dict[node] = 1

	counter = Counter(in_dict.values())
	dictionary = dict(counter)
	new_dict = {}
	for item in dictionary:
		name = str(item) + "-in"
		print name
		new_dict[name] = dictionary[item]
	return new_dict

def get_out_nodes_dictionary(G):
	out_dict = {}
	nodes = list(G.nodes())
	out_edges = G.out_edges()
	for node in nodes:
		for out_edge in out_edges:
			if out_edge[1] == node:
				if node in out_dict:
					out_dict[node] +=1
				else:
					out_dict[node] = 1

	counter = Counter(out_dict.values())
	dictionary = dict(counter)
	new_dict = {}
	for item in dictionary:
		name = str(item) + "-out"
		print name
		new_dict[name] = dictionary[item]
	return new_dict