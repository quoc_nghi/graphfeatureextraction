import pandas as pd
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import svm
from sklearn import linear_model
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import VarianceThreshold
from sklearn.multiclass import OneVsOneClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn import linear_model
from sklearn.externals import joblib
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from utils import plot_confusion_matrix
from sklearn.metrics import classification_report
from utils import plot_classification_report
import numpy as np
import matplotlib.pyplot as plt
import xgboost as xgb

# df = pd.read_csv("deep_features1234567.txt", header = None)
# df = pd.read_csv("final_features.txt", header = None)

FIGURE = "../figure/"
def perf_measure(y_actual, y_true):
    TP = 0
    FP = 0
    TN = 0
    FN = 0

    for i in range(len(y_hat)): 
        if y_actual[i]==y_hat[i]==1:
           TP += 1
    for i in range(len(y_hat)): 
        if y_hat[i]==1 and y_actual!=y_hat[i]:
           FP += 1
    for i in range(len(y_hat)): 
        if y_actual[i]==y_hat[i]==0:
           TN += 1
    for i in range(len(y_hat)): 
        if y_hat[i]==0 and y_actual!=y_hat[i]:
           FN += 1

	return TP, FP, TN, FN

def VarianceThresHold_selector(data):
	columns = data.columns
	selector = VarianceThreshold(threshold=(.1*(1-.8)))
	selector.fit_transform(data)
	labels = [columns[x] for x in selector.get_support(indices=True) if x]
	return pd.DataFrame(selector.fit_transform(data),columns=labels)

# df = pd.read_csv("../features/features_20_classes_with_id_1.csv")
# df = pd.read_csv("../features/final_features_with_sample_id_full.txt")
df = pd.read_csv("../features/sample_joins4.csv")


print len(list(df.columns.values))
print df.shape
# print df
# features = list(df.columns[1:558])
features = list(df.columns[1:614])
print features
# print features
y = df["label"]

# print y

# print y
X = df[features]


X = VarianceThresHold_selector(X)
# X = VarianceThreshold(threshold=(.1*(1-.8))).fit_transform(X)
# features2 = list(X.columns[:27])
# features2 = list(X.columns[27:37])

# print X.columns
# X = X[features2]
# print X
# print X
# print df.columns.tolist()
# print X.shape
print "Finished feature selection"


clf_random_forest = RandomForestClassifier(n_estimators=100, random_state=1).fit(X, y)
importances = clf_random_forest.feature_importances_
std = np.std([tree.feature_importances_ for tree in clf_random_forest.estimators_],
         axis=0)
indices = np.argsort(importances)[-50:]
# print indices
# print X.shape[1]
all_features = X.columns
features_names = []
for index in indices:
  features_names.append(all_features[index])
print features_names
print("Feature ranking:")
for f in range(len(indices)):
  # feature_name = features_names[f]
  # print feature_name
  print("%d. feature %s (%f)" % (f + 1, features_names[f], importances[indices[f]]))
# plt.figure()
# plt.title("Feature ranking", fontsize = 20)
# plt.bar(range(len(indices)), importances[indices],color="b", align="center")



# # print features_names
# print "length : " + str(features_names)
# plt.xticks(range(len(indices)), features_names)
# plt.xlim([-1,len(indices)])
# plt.ylabel("importance", fontsize = 18)
# plt.xlabel("index of the feature", fontsize = 18)
# plt.show()