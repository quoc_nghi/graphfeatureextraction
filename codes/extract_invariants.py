import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
from networkx.drawing import nx_agraph
# from utils import extract_variants
import os
from subprocess import call
from sets import Set
import itertools
import copy

CFG_PATH = "/Users/quocnghi/python/graphfeatureextraction/"
# CFG_PATH = "/home/quocnghi/courses/software_mining/extraction/"
callgraph_path = CFG_PATH + "CFGCallGraphData_sample/1/18/callgraph.dot"
cfg_path = CFG_PATH + "CFGCallGraphData_sample/1/40/cfg._Z1fii.dot"
print callgraph_path



# G=nx.MultiDiGraph(nx_pydot.read_dot(callgraph_path))
# nodes = list(G.nodes())
# # print nodes

def is_belong_to(text,str_arr):
	for s in str_arr:
		if text == s:
			return True
	return False

with open(cfg_path,"r") as f:
	data = f.read()
# print data
G=nx_agraph.from_agraph(pygraphviz.AGraph(data))
# G=nx.MultiDiGraph(nx_pydot.read_dot(cfg_path))
G1 = nx.convert_node_labels_to_integers(G)
# nx.write_dot(G,"temp.dot")
nodes = list(G.nodes())
nodes1 = list(G1.nodes())

# print nodes1
# print nodes
print nodes

cycle_edges = list(nx.simple_cycles(G))

print "Cycle edges : " + str(cycle_edges)



for n,d in G.nodes_iter(data=True):
	if "label" in d:
		if "main" in d["label"]:
			print n
			main_node = n

main_node =""
branch_nodes = []
start_node = ""
cfg_nodes_dictionary = {}
for n,d in G.nodes_iter(data=True):
	cfg_nodes_dictionary[n] = d

# print cfg_nodes_dictionary


for n,d in G.nodes_iter(data=True):

	if "label" in d:
		# match = re.search(r'T(.*)F:', d["label"])
		if "T" in d["label"] and "F" in d["label"] and "|" in d["label"]:
		# if match is not None:
			branch_nodes.append(n)
		if "%0" in d["label"]:
			start_node = n

print "Branch nodes :" + str(branch_nodes)

def check_if_a_parent_of_b(a,b):
	print "checking if " + a  + " is parent of " + b
	label_a = cfg_nodes_dictionary[a]["label"]
	label_b = cfg_nodes_dictionary[b]["label"]
	label_a = str(label_a)
	
	match_obj_a = re.search(r'%(.*):', label_a)
	match_obj_b = re.search(r'%(.*):', label_b)
	instruction_a = re.findall(r'\d+',match_obj_a.group())
	instruction_b = re.findall(r'\d+',match_obj_b.group())
	print a + " ins : " + instruction_a[0]
	print b + " ins : " + instruction_b[0]
	if int(instruction_a[0]) < int(instruction_b[0]):
		print a + " is parent of " + b
		return True
	print a + " is not parent of " + b
	return False


print "Getting model--------"

# This part is to detect if a node is for or an if node
def get_trait_dicts(branch_nodes):
	branch_nodes_trait_dict = {}
	print "cccc : " + str(cycle_edges)
	if len(branch_nodes) == 2 and len(cycle_edges) == 1:
		if branch_nodes[0] in cycle_edges[0] and branch_nodes[1] in cycle_edges[0]:
			check  = check_if_a_parent_of_b(branch_nodes[0],branch_nodes[1])
			if check == True:
				branch_nodes_trait_dict[branch_nodes[0]] = "for"
			else:
				branch_nodes_trait_dict[branch_nodes[1]] = "for"
	else:
		for branch_node in branch_nodes:

			paths_of_current_node = list(nx.all_simple_paths(G,source=branch_node,target=branch_node))
			
			for path in paths_of_current_node:
				node_before_last_node = path[len(path)-2]
				print "node before last node : " + node_before_last_node
				if node_before_last_node in branch_nodes:
					print node_before_last_node + " is a if"
					branch_nodes_trait_dict[branch_node] = "if"
				else:
					print node_before_last_node + " is a for"
					branch_nodes_trait_dict[branch_node] = "for"
	return branch_nodes_trait_dict

branch_nodes_trait_dict = get_trait_dicts(branch_nodes)
print "Branch trait : " + str(branch_nodes_trait_dict)


def get_nested_dict(branch_nodes_trait_dict):
	branch_nodes_nested_dict = {}

	if len(branch_nodes_trait_dict) == 1:
		node = {
			"node_type": branch_nodes_trait_dict[next(iter(branch_nodes_trait_dict))]
		}
		branch_nodes_nested_dict[next(iter(branch_nodes_trait_dict))] = node

	else:

		for a,b in itertools.combinations(branch_nodes_trait_dict.keys(),2):
			print "souce a : " + a
			print "souce b : " + b
			paths_of_node_a = list(nx.all_simple_paths(G,source=a,target=a))
			print "Doing for a -----------------------------------"
			print "path of source a"
			check = check_if_a_parent_of_b(a,b)
			for path in paths_of_node_a:
				print "################"

				print path
				if b in path and check == True:
					print b + " in path of " + a
					if a in branch_nodes_nested_dict:
						if "nodes" in branch_nodes_nested_dict[a]:
							nodes = branch_nodes_nested_dict[a]["nodes"]
							node_obj = {
								"nodes": nodes
							}
							print "node obj : " + str(node_obj)
							new_node = {
								b : {
									"node_type": branch_nodes_trait_dict[b]
								}
							}
							node_obj["nodes"].update(new_node)
							node_obj.update({"node_type":branch_nodes_trait_dict[a]})
							print "node obj : " + str(node_obj)
							node_to_update = {
								a : node_obj
							}
						else:
							node_obj = {
								"nodes": {}
							}
							print "node obj : " + str(node_obj)
							new_node = {
								b : {
									"node_type": branch_nodes_trait_dict[b]
								}
							}
							node_obj["nodes"].update(new_node)
							node_obj.update({"node_type":branch_nodes_trait_dict[a]})
							print "node obj : " + str(node_obj)
							node_to_update = {
								a : node_obj
							}
						print "here 3"
					else:
						sub_type = {
							"node_type":branch_nodes_trait_dict[b],
						}
						parent_type = {
							"node_type" : branch_nodes_trait_dict[a],
							"nodes" : {
								b : sub_type
							}		
						}
						print "parent type : " + str(parent_type)
						print "here 1"
						node_to_update = {
							a : parent_type
						}
						print "here 4"
					print "node to update : " + str(node_to_update)
					branch_nodes_nested_dict.update(node_to_update)

				
				else:
					node = {
						"node_type": branch_nodes_trait_dict[a]
					}
					branch_nodes_nested_dict[a] = node
				print "branch node nested dict after a " + str(branch_nodes_nested_dict)
			print "Doing for b -----------------------------------"
			paths_of_node_b = list(nx.all_simple_paths(G,source=b,target=b))
			
			print "path of source b"
			check = check_if_a_parent_of_b(b,a)
			for path in paths_of_node_b:
				print "################"
				print path
				if a in path and check == True:
					print a + " in path of " + b
					if b in branch_nodes_nested_dict:
						if "nodes" in branch_nodes_nested_dict[b]:
							nodes = branch_nodes_nested_dict[b]["nodes"]
							node_obj = {
								"nodes": nodes
							}
							print "node obj : " + str(node_obj)
							new_node = {
								a : {
									"node_type": branch_nodes_trait_dict[a]
								}
							}
							node_obj["nodes"].update(new_node)
							node_obj.update({"node_type":branch_nodes_trait_dict[b]})
							print "node obj : " + str(node_obj)
							node_to_update = {
								b : node_obj
							}
						else:
							node_obj = {
								"nodes": {}
							}
							print "node obj : " + str(node_obj)
							new_node = {
								a : {
									"node_type": branch_nodes_trait_dict[a]
								}
							}
							node_obj["nodes"].update(new_node)
							node_obj.update({"node_type":branch_nodes_trait_dict[b]})
							print "node obj : " + str(node_obj)
							node_to_update = {
								b : node_obj
							}
						print "here 5"
					else:
						sub_type = {
							"node_type":branch_nodes_trait_dict[a],
						}
						parent_type = {
							"node_type" : branch_nodes_trait_dict[b],
							"nodes" : {
								a : sub_type
							}		
						}
						print "parent type : " + str(parent_type)
						print "here 6"
						node_to_update = {
							b : parent_type
						}
						print "here 7"
					print "node to update : " + str(node_to_update)
					branch_nodes_nested_dict.update(node_to_update)
				else:
					node = {
						"node_type": branch_nodes_trait_dict[b]
					}
					branch_nodes_nested_dict[b] = node
				print "branch node nested dict after b " + str(branch_nodes_nested_dict)
	return branch_nodes_nested_dict
print "###############"
branch_nodes_nested_dict = get_nested_dict(branch_nodes_trait_dict)
print "branch_nodes_nested_dict : " + str(branch_nodes_nested_dict)
print "###############"


# Merge nodes
# branch_nodes_hierarchy_dict = copy.deepcopy(branch_nodes_nested_dict)
def get_branch_nodes_hierarchy_dict(branch_nodes_nested_dict):
	branch_nodes_hierarchy_dict = copy.deepcopy(branch_nodes_nested_dict)
	for a,b in itertools.combinations(branch_nodes_nested_dict.keys(),2):	
		if "nodes" in branch_nodes_nested_dict[a] and "nodes" in branch_nodes_nested_dict[b]:
			

			a_child = branch_nodes_nested_dict[a]["nodes"]
			b_child = branch_nodes_nested_dict[b]["nodes"]

			for key,value in a_child.iteritems():
				if key == b:
					node = {
						b : branch_nodes_nested_dict[b]
					}
					branch_nodes_hierarchy_dict[a]["nodes"].update(node)
					branch_nodes_hierarchy_dict.pop(b,None)
			for key,value in b_child.iteritems():
				if key == a:
					node = {
						a : branch_nodes_nested_dict[a]
					}
					branch_nodes_hierarchy_dict[b]["nodes"].update(node)
					branch_nodes_hierarchy_dict.pop(a,None)
	return branch_nodes_hierarchy_dict

branch_nodes_hierarchy_dict = get_branch_nodes_hierarchy_dict(branch_nodes_nested_dict)
print branch_nodes_hierarchy_dict		



# for branch_node, value in branch_nodes_trait_dict.iteritems():
# 	print branch_node
# 	if value == "for":
# 		paths_of_current_node = list(nx.all_simple_paths(G,source=branch_node,target=branch_node))
# 		for path in paths_of_current_node:

# for branch_node in branch_nodes:
# 	print "Current branch node : " + branch_node + " $$$$$$$$$$$$$$$$$$$$$$$$$$$-------------------------"
# 	for node in nodes:
# 		paths = list(nx.all_simple_paths(G,source=branch_node,target=node))
# 		print paths
# 		if len(paths) != 0:

# 			for path in paths:
# 				print "Printing path"
# 				print path
# 				index = 0
# 				for node_index in range(1,len(path)):
# 					# if index == 0:
# 					# 	continue
# 					# print node_in_path
# 					full_node = cfg_nodes_dictionary[path[node_index]]

# 					if "T" in full_node["label"] and "F" in full_node["label"]:
# 						if path[0] != path[node_index]:
# 							temp = path[0] + "_" + path[node_index]
# 							pattern_1_set.add(temp)
# 							pattern_1 += 1

# print pattern_1_set
# print len(pattern_1_set)
# print pattern_1









# print nx.dag_longest_path(G)
# print "#pathhhhhhhh"
# paths = nx.all_simple_paths(G,source="Node0x55ae8fe803b0",target="Node0x55ae8fe7f2a0")
# print list(paths)
# nx.draw(G1,with_labes=True)
# nx.draw_networkx_labels(G1,pos=nx.spring_layout(G1))
# plt.show()
