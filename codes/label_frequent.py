import pandas as pd
from itertools import groupby
from scipy.stats import itemfreq
import numpy as np
df = pd.read_csv("features_7_classes_2.txt", header = None)

features = list(df.columns[:557])

y = df[557]

set_y = list(set(y))

freq = itemfreq(y)

for item in freq:
	line = str(item[0]) + "," + str(item[1])
	with open("frequent_7_classes_2.txt", "a") as f:			
		f.write(line + "\n")
