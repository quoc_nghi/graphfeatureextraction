import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
from networkx.drawing import nx_agraph
# from utils import extract_variants
import os
from subprocess import call
from extract_dominants import extract_patterns
# count_loop, count_condition = extract_variants("../sample_cpp/1001.cpp")
ROOT_URL = "/home/quocnghi/courses/software_mining/extraction/";
ROOT_URL = "/Users/quocnghi/python/graphfeatureextraction/"

source = os.listdir(os.path.join(ROOT_URL,"CFGCallGraphData"))

for dir in source:
	print dir
	dir_path = os.path.join(ROOT_URL,"CFGCallGraphData",dir)
	
	for root,dirs,files in os.walk(dir_path):
		for subdir in dirs:
			
			index = dir + "_" + subdir
			subdir_path = os.path.join(root,subdir)
			# print list_files
			list_files = os.listdir(subdir_path)
			# for graph_file in list_files:
			print subdir_path
			original_patterns_dict = {
				"for-seq-for": 0,
				"for-seq-if": 0,
				"for-nested-if": 0,
				"for-nested-for": 0,
				"if-seq-if": 0,
				"if-seq-for": 0,
				"if-nested-for": 0,
				"if-nested-if": 0,
			}
			for graph_file in os.listdir(subdir_path):
				
				if graph_file != "callgraph.dot" and graph_file != "cfg.__cxx_global_var_init.dot" and "GLOBAL__sub" not in graph_file:
					print graph_file
					graph_file_path = os.path.join(subdir_path,graph_file)
					print graph_file_path
					with open(graph_file_path,"r") as f:
						data = f.read()
					G = nx_agraph.from_agraph(pygraphviz.AGraph(data))
					print list(G.nodes())
					
					patterns_dict = extract_patterns(G)
					original_patterns_dict.update(patterns_dict)
			print original_patterns_dict
			sorted_merge = collections.OrderedDict(sorted(original_patterns_dict.items()))
			print sorted_merge
			line = ",".join(str(v) for v in sorted_merge.values())
			line = line + "," + str(dir)
			line_with_index = index + "," + line
			with open("pattern_features.txt", "a") as f:	
				print line_with_index
				f.write(line_with_index + "\n")
				