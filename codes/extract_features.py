import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
from networkx.drawing import nx_agraph
import util

	
wb = openpyxl.load_workbook('../features_new.xlsx')

worksheet = wb.get_sheet_by_name('Sheet1')
all_features = list()

for row in worksheet.iter_rows('A1:UK'.format(worksheet.min_row,worksheet.max_row)):
    for cell in row:
    	if cell.value:
    		all_features.append(cell.value)

print "all features length : " + str(len(all_features))
with open("../sample/100/1561.bc.dot", "r") as ins:
	data=ins.read()


G=nx_agraph.from_agraph(pygraphviz.AGraph(data))
# G=nx.MultiDiGraph(nx_pydot.read_dot("./DotData/1/1162.dot"))
# print(list(G.nodes()))



G = nx.convert_node_labels_to_integers(G)
nodes = list(G.nodes())
in_edges = G.in_edges()
out_edges = G.out_edges()

print nodes
# print in_edges
# print out_edges
# nx.draw(G)
# plt.show(G)

in_dict = {}
out_dict = {}

print "#######################"
for node in nodes:
	for in_edge in in_edges:
		if in_edge[1] == node:
			if node in in_dict:
				in_dict[node] +=1
			else:
				in_dict[node] = 1

	for out_edge in out_edges:
		if out_edge[0] == node:
			if node in out_dict:
				out_dict[node] +=1
			else:
				out_dict[node] = 1
# print in_dict
# print out_dict
keys_in = set(in_dict.keys())
keys_out = set(out_dict.keys())
intersection = keys_in | keys_out
# print intersection

print "#######################"

pq_nodes_dict = util.get_pq_nodes_dictionary(intersection,out_dict,in_dict)


print pq_nodes_dict

for feature in all_features:
	if feature not in pq_nodes_dict:
		pq_nodes_dict[feature] = 0
# print pq_nodes_dict
# sorted_pq_nodes_dict = collections.OrderedDict(sorted(pq_nodes_dict.items()))

# print sorted_pq_nodes_dict

print "#######################"
paths_dict = {}
for i in range(len(nodes)):
    for j in range(i + 1, len(nodes)):
        for path in nx.all_simple_paths(G, source=nodes[i], target=nodes[j]):
        	path_length = len(path)
        	if path_length in paths_dict:
        		paths_dict[str(path_length) + "-path"] += 1
        	else:
        		paths_dict[str(path_length) + "-path"] = 1

print paths_dict

print "#######################"
merge = pq_nodes_dict.copy()
merge.update(paths_dict)
print merge

degree_dict = util.get_degree_dictionary(G)

degree_features_dict = util.get_degree_features_dictionary(degree_dict)

print degree_features_dict


print "#######################"

in_nodes_dict = util.get_in_nodes_dictionary(G)
out_nodes_dict = util.get_out_nodes_dictionary(G)

merge.update(degree_features_dict)
merge.update(in_nodes_dict)
merge.update(out_nodes_dict)

print merge

print len(merge)
# sorted_merge = collections.OrderedDict(sorted(merge.items()))

# print sorted_merge
# print "all sorted merge : "
# print len(sorted_merge)

# print sorted_merge.values()