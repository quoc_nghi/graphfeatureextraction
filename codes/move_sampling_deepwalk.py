import os

url = "/home/quocnghi/courses/software_mining/extraction/"
# url = "/Users/quocnghi/python/graphfeatureextraction/"
for root,dirs,files in os.walk(url + "DeepWalkData"):
	for dir in dirs:
		dir_path = os.path.join(root,dir)
		index = 0
		for filename in os.listdir(dir_path):
			if index > 5:
				break;
			old_path = os.path.join(dir_path,filename)
			new_path = url + "DeepWalkData_sampling/" + dir + "/" + filename 
			print old_path
			print new_path
			os.rename(old_path,new_path)
			index += 1