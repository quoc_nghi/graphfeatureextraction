import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
from networkx.drawing import nx_agraph
# from utils import extract_variants
import os
from subprocess import call

# count_loop, count_condition = extract_variants("../sample_cpp/1001.cpp")
ROOT_URL = "/home/quocnghi/courses/software_mining/extraction/";
for root,dirs,files in os.walk(ROOT_URL + "ProgramData_sample"):
	
	
	index = 0
	for file in files:
		path = os.path.join(root,file)
		print path

		split = path.split("/")
		# Example : split[7] = 37
		index = split[7] + "a" + split[8].split(".")[0]
		print index
		label = split[7]
		call_graph_command = "clang -c -S -emit-llvm " + path + " -o -| opt -dot-callgraph -o /dev/null"
		os.system(call_graph_command)
		cfg_command = "clang -c -S -emit-llvm " + path + " -o -| opt -dot-cfg -o /dev/null"
		os.system(cfg_command)
		cfg_directory = ROOT_URL + "CFGCallGraphData/" + split[7] + "/" + split[8].split(".")[0] 
		if not os.path.exists(cfg_directory):
			os.makedirs(cfg_directory)

		print cfg_directory
		source_path = ROOT_URL + "codes"
		source = os.listdir(source_path)
		for file in source:
			if file.endswith(".dot"):	
				os.rename(os.path.join(source_path,file),os.path.join(cfg_directory,file))

		# call(["clang","-c","-S","-emit-llvm",path,"-o","|","opt","-dot-callgraph","-o","/dev/null"])
		
		