import os
import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
import numpy as np
from networkx.drawing import nx_agraph
from networkx.linalg import graphmatrix
import util
import errno

url = "/home/quocnghi/courses/software_mining/extraction/"

path = url + "DotData/104/100.bc.dot" 
with open(path, "r") as ins:
	data=ins.read()
	
	G=nx_agraph.from_agraph(pygraphviz.AGraph(data))
	G = nx.convert_node_labels_to_integers(G)
	print len(list(G.nodes()))
	adjacency_directory = url + "AdjacencyData/100.txt" 
	for line in nx.generate_adjlist(G):
		print line
		with open(adjacency_directory,"a+") as append:
			append.write(line +"\n")
	# print adjacency_list
	# # print adjacency_matrix.toarray()
	# # print file
	
	
	
	# np.savetxt(adjacency_directory + "100" + ".txt",adjacency_matrix.toarray().astype(int),fmt="%i")

