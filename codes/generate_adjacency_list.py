import os
import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
import numpy as np
from networkx.drawing import nx_agraph
from networkx.linalg import graphmatrix
import util
import errno

# with open("../sample/100/1579.bc.dot", "r") as ins:
# 	data=ins.read()
url = "/home/quocnghi/courses/software_mining/extraction/"
# url = "/Users/quocnghi/python/graphfeatureextraction/"
for root,dirs,files in os.walk(url + "DotData"):
	for file in files:
		path = os.path.join(root,file)

		# print path
		# print path.split("/")[7]
		with open(path, "r") as ins:
			data=ins.read()
		try:
			G=nx_agraph.from_agraph(pygraphviz.AGraph(data))	
			G = nx.convert_node_labels_to_integers(G)	
			adjacency_directory = url + "AdjacencyListData/" + path.split("/")[7] + "/"
			print adjacency_directory

			for line in nx.generate_adjlist(G):		
				print line
				adjacency_list_file = adjacency_directory + file.split(".")[0] + ".txt"
				print adjacency_list_file
				try:
					os.makedirs(adjacency_directory)
				except OSError as exception:
					print "already existed"
				with open(adjacency_list_file,"a+") as append:
						append.write(line +"\n")				
		except:
			continue