import pandas as pd
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import svm
from sklearn import linear_model
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import VarianceThreshold
from sklearn.multiclass import OneVsOneClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn import linear_model
from sklearn.externals import joblib
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from utils import plot_confusion_matrix
from sklearn.metrics import classification_report
from utils import plot_classification_report
import numpy as np
import matplotlib.pyplot as plt
import xgboost as xgb

df1 = pd.read_csv("../features/final_features_with_id_full.csv")
indexes_df1 = df1.set_index(["id"])
# df2 = pd.read_csv("../features/pattern_features_all_relax_big_graph.csv")

df2 = pd.read_csv("../features/pattern_features_all_extra_relax_big_graph4.txt")
indexes_df2 = df2.set_index(["id"])

# result = pd.concat([indexes_df1,indexes_df2],axis=1)
result = pd.merge(df1,df2,on="id")
del result["label_x"]
# result = df1.append(df2)
result.to_csv("../features/sample_joins4.csv",sep=",",encoding="utf-8",index=False)