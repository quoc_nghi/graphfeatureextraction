import pandas as pd
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import svm
from sklearn import linear_model
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import VarianceThreshold
from sklearn.multiclass import OneVsOneClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn import linear_model
from sklearn.externals import joblib
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from utils import plot_confusion_matrix
from sklearn.metrics import classification_report
from utils import plot_classification_report
import numpy as np
# import matplotlib.pyplot as plt
# import xgboost as xgb
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectFromModel
from sklearn import preprocessing
from sklearn.preprocessing import Imputer

# df = pd.read_csv("deep_features1234567.txt", header = None)
# df = pd.read_csv("final_features.txt", header = None)

FIGURE = "../figure/"


def VarianceThresHold_selector(data):
	columns = data.columns
	selector = VarianceThreshold(threshold=(.8*(1-.8)))
	selector.fit_transform(data)
	labels = [columns[x] for x in selector.get_support(indices=True) if x]
	return pd.DataFrame(selector.fit_transform(data),columns=labels)

# df = pd.read_csv("../features/features_20_classes_with_id_1.csv")
# df = pd.read_csv("../features/final_features_with_id_full.csv")
# df = pd.read_csv("../features/final_features_with_sample_id_full_multiple_graphs3.txt")
# df = pd.read_csv("../features/semantics_machine_code_features_dbn_with_index.csv")
df = pd.read_csv("../features/all_handcrafted_features.csv")
df = df.apply(pd.to_numeric,args=("coerce",))

# df = df.loc[df["label"].isin([20,28])]
# df = df.iloc[0:12000]
print len(list(df.columns.values))
print df.shape
# print df
# features = list(df.columns[1:558])
features = list(df.columns[1:647])
# features = list(df.columns[1:101])
print features
# print features
y = df["label"]

# print y

# print y
X = df[features]
# imputer = Imputer(missing_values="NaN",strategy="mean",axis=0)
# imputer.fit(X)
# X = imputer.fit_transform(X)
# clf = RandomForestClassifier(n_estimators=100, random_state=1).fit(X, y)
# model = SelectFromModel(clf, prefit=True)
# X = model.transform(X)
# X = SelectKBest(chi2,k=2).fit_transform(X,y)
# X = VarianceThresHold_selector(X)
X = VarianceThreshold(threshold=(.1*(1-.8))).fit_transform(X)
# features2 = list(X.columns[:27])
# features2 = list(X.columns[27:37])
# X = preprocessing.normalize(X,norm="l2")
X = preprocessing.scale(X)
# print X.columns
# X = X[features2]
print X
# print X
# print df.columns.tolist()
print X.shape
print "Finished feature selection"

skf = StratifiedKFold(n_splits=10,random_state=1)
skf.get_n_splits(X,y)

# X_train, X_test, y_train, y_test = train_test_split(X,y,test_size = 0.3, random_state = 42)

index = 0
random_forest_precisions = []
extra_tree_precisions = []
voting_precisions = []
for train_index, test_index in skf.split(X,y):
	print "########################"
	X_train, X_test = X[train_index], X[test_index]
	# X_train, X_test = X.iloc[train_index], X.iloc[test_index]
	y_train, y_test = y[train_index], y[test_index]


	# X_test.to_csv("../features/X_test.csv",sep=",",encoding="utf-8",index=False)
	print "Size of X_train and X test  :" + str(X_train.shape) + str(X_test.shape)
	# if index > 0:
	# 	break
	# forest = RandomForestClassifier(n_estimators=100, random_state=1)
	clf_random_forest = RandomForestClassifier(n_estimators=150, random_state=1).fit(X_train, y_train)
	# clf_random_forest = OneVsRestClassifier(forest,n_jobs = -1).fit(X_train, y_train)
	# class_names = clf_rest.classes_
	# class_names_str = map(str,class_names)
	# print class_names_str

	y_pred_random_forest_rest  = clf_random_forest.predict(X_test);

	# cnf_matrix = confusion_matrix(y_test, y_pred_random_forest_rest)
	# classfy_report = classification_report(y_test,y_pred_random_forest_rest,target_names=class_names_str)

	# print classfy_report
	# plt.figure()
	# plot_confusion_matrix(cnf_matrix, classes=class_names,
	#                       title='Confusion matrix, without normalization')
	# plt.savefig(FIGURE + "2_confusion.png", dpi=200, format='png', bbox_inches='tight')

	# plot_classification_report(classfy_report)
	# plt.savefig(FIGURE + "2_class_report", dpi=200, format='png', bbox_inches='tight')
	# plt.close()

	
	print "Precision for random forest 1 v rest : " + str(precision_score(y_test,y_pred_random_forest_rest,average = "micro"))
	print "Recall for random forest 1 v rest : " + str(recall_score(y_test,y_pred_random_forest_rest,average = "micro"))
	print "F1 score for random forest 1 v rest : " + str(f1_score(y_test,y_pred_random_forest_rest,average = "micro"))

	random_forest_precisions.append(precision_score(y_test,y_pred_random_forest_rest,average = "micro"))
	# importances = clf_random_forest.feature_importances_
	# std = np.std([tree.feature_importances_ for tree in clf_random_forest.estimators_],
 #             axis=0)
	# indices = np.argsort(importances)[::-1]

	# # Print the feature ranking
	# print("Feature ranking:")

	# for f in range(X.shape[1]):
 #   		print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))
	# clf_xgb = xgb.XGBClassifier(max_depth=12,min_child_weight=1, learning_rate =0.1,n_estimators=130,objective= 'binary:logistic',nthread=4,scale_pos_weight=1,seed=27,gamma=0.1,subsample=0.9,colsample_bytree=0.9).fit(X_train,y_train)
	# y_pred_xgb_rest  = clf_xgb_rest.predict(X_test);

	# print "Precision for xgb 1 v rest : " + str(precision_score(y_test,y_pred_xgb_rest,average = "micro"))
	# print "Recall for xgb 1 v rest : " + str(recall_score(y_test,y_pred_xgb_rest,average = "micro"))
	# print "F1 score for xgb 1 v rest : " + str(f1_score(y_test,y_pred_xgb_rest,average = "micro"))

	clf_extra_tree = ExtraTreesClassifier(n_estimators=200, random_state=1).fit(X_train, y_train)
	y_pred_extra_tree  = clf_extra_tree.predict(X_test);

	
	print "Precision for extra tree 1 v rest : " + str(precision_score(y_test,y_pred_extra_tree,average = "micro"))
	print "Recall for extra tree 1 v rest : " + str(recall_score(y_test,y_pred_extra_tree,average = "micro"))
	print "F1 score for extra tree 1 v rest : " + str(f1_score(y_test,y_pred_extra_tree,average = "micro"))
	extra_tree_precisions.append(precision_score(y_test,y_pred_extra_tree,average = "micro"))
	
	# clf_gbm = GradientBoostingClassifier(n_estimators=150, random_state=1,verbose=True).fit(X_train, y_train)
	# y_pred_gbm  = clf_gbm.predict(X_test);

	
	# print "Precision for gbm 1 v rest : " + str(precision_score(y_test,y_pred_gbm,average = "micro"))
	# print "Recall for gbm 1 v rest : " + str(recall_score(y_test,y_pred_gbm,average = "micro"))
	# print "F1 score for gbm 1 v rest : " + str(f1_score(y_test,y_pred_gbm,average = "micro"))

	# clf_lr_rest = linear_model.LogisticRegression(C=1e5,multi_class="multinomial",solver="newton-cg").fit(X_train, y_train)
	# y_pred_lr_rest  = clf_lr_rest.predict(X_test);
	# print "Precision for LR 1 v rest : " + str(precision_score(y_test,y_pred_lr_rest,average = "micro"))
	# print "Recall for LR 1 v rest: " + str(recall_score(y_test,y_pred_lr_rest,average = "micro"))
	# print "F1 score for LR 1 v rest:: " + str(f1_score(y_test,y_pred_lr_rest,average = "micro"))


	# clf1 = RandomForestClassifier(n_estimators=100, random_state=1)
	# clf2 = svm.SVC(kernel='poly', C=1)
	# clf3 = linear_model.LogisticRegression(C=1e5,multi_class="multinomial")
	# clf_voting = VotingClassifier(estimators=[('rf', clf1), ('svc', clf2), ('lr', clf3)], voting='hard').fit(X_train, y_train)
	# y_pred_voting  = clf_voting.predict(X_test);
	# print "Precision for voting : " + str(precision_score(y_test,y_pred_voting,average = "micro"))
	# print "Recall for voting : " + str(recall_score(y_test,y_pred_voting,average = "micro"))
	# print "F1 score for voting : " + str(f1_score(y_test,y_pred_voting,average = "micro"))
	index += 1
	# clf_one = OneVsOneClassifier(forest,n_jobs = -1).fit(X_train, y_train)
	# # clf3_one = joblib.load("random_forest_1v1.pkl");
	# y_pred_random_forest_one  = clf_one.predict(X_test);

# 	# print "Precision for random forest 1 v 1 : " + str(precision_score(y_pred_random_forest_one, y_test,average = "micro"))
# 	# print "Recall for random forest 1 v 1 : " + str(recall_score(y_pred_random_forest_one, y_test,average = "micro"))

# 	decision_tree = DecisionTreeClassifier(min_samples_split=20, random_state=99)
# 	clf_dt_rest = OneVsRestClassifier(decision_tree,n_jobs = -1).fit(X_train, y_train)
# 	y_pred_dt_rest  = clf_dt_rest.predict(X_test);
# 	print "Precision for DecisionTree 1 vs rest : " + str(precision_score(y_pred_dt_rest, y_test,average = "micro"))
# 	print "Recall for DecisionTree 1vs rest : " + str(recall_score(y_pred_dt_rest, y_test,average = "micro"))
# 	print "F1 score for DecisionTree 1vs rest : " + str(f1_score(y_pred_dt_rest, y_test,average = "micro"))


# 	clf_multi_perceptron = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1).fit(X_train,y_train)
# 	y_pred_multi_perceptron  = clf_multi_perceptron.predict(X_test);
# 	print "Precision for multi perceptron : " + str(precision_score(y_pred_multi_perceptron, y_test,average = "micro"))
# 	print "Recall for multi perceptron : " + str(recall_score(y_pred_multi_perceptron, y_test,average = "micro"))
# 	print "F1 score for multi perceptron : " + str(f1_score(y_pred_multi_perceptron, y_test,average = "micro"))

# 	SVC_poly = svm.SVC(kernel='poly', C=1)
# 	clf_svc_poly_rest = OneVsRestClassifier(SVC_poly).fit(X_train, y_train)
# 	y_pred_svm_poly_rest  = clf_svc_poly_rest.predict(X_test);
# 	print "Precision for SVM poly 1 v rest : " + str(precision_score(y_pred_svm_poly_rest, y_test,average = "micro"))
# 	print "Recall for SVM poly 1 v rest: " + str(recall_score(y_pred_svm_poly_rest, y_test,average = "micro"))
# 	print "F1 score for SVM poly 1 v rest: " + str(f1_score(y_pred_svm_poly_rest, y_test,average = "micro"))

	# SVC_linear = svm.SVC(kernel='linear', C=1)
	# clf_svc_linear_rest = OneVsRestClassifier(SVC_linear).fit(X_train, y_train)
	# y_pred_svm_linear_rest  = clf_svc_linear_rest.predict(X_test);
	# print "Precision for SVM linear 1 v rest : " + str(precision_score(y_pred_svm_linear_rest, y_test,average = "micro"))
	# print "Recall for SVM linear 1 v rest: " + str(recall_score(y_pred_svm_linear_rest, y_test,average = "micro"))

	# clf_nb_rest = GaussianNB().fit(X_train,y_train)
	# y_pred_nb_rest  = clf_nb_rest.predict(X_test);
	# print "Precision for NB 1 v rest : " + str(precision_score(y_pred_nb_rest, y_test,average = "micro"))
	# print "Recall for NB 1 v rest: " + str(recall_score(y_pred_nb_rest, y_test,average = "micro"))

np_random_forest_precisions = np.array(random_forest_precisions)
np_extra_tree_precisions = np.array(extra_tree_precisions)

print "Final precision random forest : " + str(np.mean(np_random_forest_precisions))
print "Final precision extra forest : " + str(np.mean(np_random_forest_precisions))
