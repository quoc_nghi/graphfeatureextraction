import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
from collections import Counter
from networkx.drawing import nx_agraph
from networkx.drawing.nx_pydot import write_dot
# from utils import extract_variants
import os
from subprocess import call
from sets import Set
import itertools
import copy
import operator
import collections

BASE_PATH = "/Users/quocnghi/python/graphfeatureextraction/"
# BASE_PATH = "/home/quocnghi/courses/software_mining/extraction/"
CALLGRAPH_PATH = BASE_PATH + "CFGCallGraphData_sample/1/18/callgraph.dot"
# CFG_PATH = BASE_PATH + "CFGCallGraphData_sample/1/25/cfg._Z3fffiii.dot"
CFG_PATH = BASE_PATH + "CFGCallGraphData_sample2/27/1711/cfg.main.dot"


def get_trait_of_nodes(branch_nodes,G):
	branching_nodes_trait_dict = {}
	for branch_node in branch_nodes:

		paths_of_current_node = list(nx.all_simple_paths(G,source=branch_node,target=branch_node))
		if len(paths_of_current_node) == 0:
			branching_nodes_trait_dict[branch_node] = "if"
		else:
			for path in paths_of_current_node:
				node_before_last_node = path[len(path)-2]
				# print "node before last node : " + node_before_last_node
				if node_before_last_node in branch_nodes:
					# print node_before_last_node + " is a if"
					branching_nodes_trait_dict[branch_node] = "if"
				else:
					# print node_before_last_node + " is a for"
					branching_nodes_trait_dict[branch_node] = "for"
	return branching_nodes_trait_dict

def conduct_nodes_dictionary(G):
	cfg_nodes_dictionary = {}
	for n,d in G.nodes_iter(data=True):
		cfg_nodes_dictionary[n] = d
	return cfg_nodes_dictionary

def get_start_and_end_node(cfg_nodes_dictionary):
	
	start_node = ""
	end_node_value = 0
	end_node = ""
	for key,value in cfg_nodes_dictionary.iteritems():
		label = cfg_nodes_dictionary[key]["label"]
		match_obj = re.search(r'%(.*):', label)
		instruction = re.findall(r'\d+',match_obj.group())
		if int(instruction[0]) == 0:
			start_node = key
		if int(instruction[0]) > end_node_value:
			end_node_value = int(instruction[0])
			end_node = key
	return start_node,end_node


def get_branching_nodes(G):
	branching_nodes = list()
	for n,d in G.nodes_iter(data=True):
		if "label" in d:
			if "T" in d["label"] and "F" in d["label"] and "|" in d["label"]:
				branching_nodes.append(n)
	return branching_nodes		
	

# check if a dominates b
def dominates(a,b,start,G):
	paths_from_start_to_b = list(nx.all_simple_paths(G,source=start,target=b))
	count = 0
	for path in paths_from_start_to_b:
		if a in path:
			count += 1
	if count == len(paths_from_start_to_b):
		return True
	return False


# check if b postdominates a
def post_dominates(b,a,end,G):
	paths_from_a_to_end = list(nx.all_simple_paths(G,source=a,target=end))

	count = 0
	for path in paths_from_a_to_end:
		if b in path:
			count += 1
	if count == len(paths_from_a_to_end):
		return True
	return False

# check if a is sequential with b or if a contains b, means that b is nested inside a
# return 1 is a is sequential to b , return 0 if b is nested inside a
def check_relationship(a,b,start,end,G):
	# print "checking : " + a + " and " + b
	d = dominates(a,b,start,G)
	p = post_dominates(b,a,end,G)
	
	# print d,p
	if d == True and p == False:
		return 0
	if d == True and p == True:
		return 1
	return 2

def remove_all_zero_degree_nodes(G):
	zero_degree_nodes = nx.isolates(G)
	for node in zero_degree_nodes:
		G.remove_node(node)
	return G

def get_sorted_nodes_dictionary(G):
	cf_nodes_dictionary = conduct_nodes_dictionary(G)
	node_order_dict = collections.OrderedDict()
	temp_dict = {}
	for n,d in G.nodes_iter(data=True):
		label = d["label"]
		match_obj = re.search(r'%(.*):', label)
		instruction = re.findall(r'\d+',match_obj.group())
		temp_dict[n] = int(instruction[0])
	sorted_tuple = sorted(temp_dict.items(), key=operator.itemgetter(1))
	print sorted_tuple
	for ele in sorted_tuple:
		print ele
		node_order_dict[ele[0]] = cf_nodes_dictionary[ele[0]]
	return node_order_dict

def extract_patterns2(G):

	cfg_nodes_dictionary = conduct_nodes_dictionary(G)
	start_node,end_node = get_start_and_end_node(cfg_nodes_dictionary)
	branching_nodes = get_branching_nodes(G)
	branching_nodes_trait_dict = get_trait_of_nodes(branching_nodes,G)
	# print "branching_nodes : " + str(branching_nodes)
	# print "branching_nodes_trait_dict : " + str(branching_nodes_trait_dict)
	patterns_dict = {
		"for-1-paths-seq-for": 0,
		"for-1-paths-seq-if": 0,
		"for-1-paths-nested-if": 0,
		"for-1-paths-nested-for": 0,
		"if-1-paths-seq-if": 0,
		"if-1-paths-seq-for": 0,
		"if-1-paths-nested-for": 0,
		"if-1-paths-nested-if": 0,

		"for-2-paths-seq-for": 0,
		"for-2-paths-seq-if": 0,
		"for-2-paths-nested-if": 0,
		"for-2-paths-nested-for": 0,
		"if-2-paths-seq-if": 0,
		"if-2-paths-seq-for": 0,
		"if-2-paths-nested-for": 0,
		"if-2-paths-nested-if": 0,

		"for-3-paths-seq-for": 0,
		"for-3-paths-seq-if": 0,
		"for-3-paths-nested-if": 0,
		"for-3-paths-nested-for": 0,
		"if-3-paths-seq-if": 0,
		"if-3-paths-seq-for": 0,
		"if-3-paths-nested-for": 0,
		"if-3-paths-nested-if": 0,

		"for-4-paths-seq-for": 0,
		"for-4-paths-seq-if": 0,
		"for-4-paths-nested-if": 0,
		"for-4-paths-nested-for": 0,
		"if-4-paths-seq-if": 0,
		"if-4-paths-seq-for": 0,
		"if-4-paths-nested-for": 0,
		"if-4-paths-nested-if": 0,

		"for-5-paths-seq-for": 0,
		"for-5-paths-seq-if": 0,
		"for-5-paths-nested-if": 0,
		"for-5-paths-nested-for": 0,
		"if-5-paths-seq-if": 0,
		"if-5-paths-seq-for": 0,
		"if-5-paths-nested-for": 0,
		"if-5-paths-nested-if": 0,

		"for-6-paths-seq-for": 0,
		"for-6-paths-seq-if": 0,
		"for-6-paths-nested-if": 0,
		"for-6-paths-nested-for": 0,
		"if-6-paths-seq-if": 0,
		"if-6-paths-seq-for": 0,
		"if-6-paths-nested-for": 0,
		"if-6-paths-nested-if": 0,
	}
	for a,b in itertools.combinations(branching_nodes_trait_dict.keys(),2):
		# print "itertoosl : " + a + "," + b
		label_a = cfg_nodes_dictionary[a]["label"]
		match_obj_a = re.search(r'%(.*):', label_a)
		instruction_a = re.findall(r'\d+',match_obj_a.group())
		label_b = cfg_nodes_dictionary[b]["label"]
		match_obj_b = re.search(r'%(.*):', label_b)
		instruction_b = re.findall(r'\d+',match_obj_b.group())

		if int(instruction_a[0]) < int(instruction_b[0]):
			node_1 = a
			node_2 = b
			
		else:
			node_1 = b
			node_2 = a
			
		relationship_index = check_relationship(node_1,node_2,start_node,end_node,G)
		
		
		# print relationship_index
		if relationship_index == 1:
			paths_2_nodes = list(nx.all_simple_paths(G,source=node_1,target=node_2))
			print paths_2_nodes
			for path in paths_2_nodes:
				path_length = len(path)
				pattern = branching_nodes_trait_dict[node_1] + "-" + str(path_length) + "-" + "paths" + "-seq-" + branching_nodes_trait_dict[node_2]
				if pattern in patterns_dict:
					patterns_dict[pattern] += 1
				else:
					patterns_dict[pattern] = 1

		if relationship_index == 0:
			paths_2_nodes = list(nx.all_simple_paths(G,source=node_1,target=node_2))
			print paths_2_nodes
			is_1_layer_nested = True
			for path in paths_2_nodes:
				for branching_node in branching_nodes:
					if branching_node in path and branching_node != node_1:
						if check_relationship(branching_node,node_2,start_node,end_node,G) == 0:
							is_1_layer_nested = False
							break;

			if is_1_layer_nested == True:
				for path in paths_2_nodes:
					path_length = len(path)
					pattern = branching_nodes_trait_dict[node_1] + "-" + str(path_length) + "-" + "paths" +"-nested-" + branching_nodes_trait_dict[node_2]
					if pattern in patterns_dict:
						patterns_dict[pattern] += 1
					else:
						patterns_dict[pattern] = 1
	return patterns_dict


def extract_patterns(G):
	G = remove_all_zero_degree_nodes(G)
	cfg_nodes_dictionary = conduct_nodes_dictionary(G)
	start_node,end_node = get_start_and_end_node(cfg_nodes_dictionary)
	branching_nodes = get_branching_nodes(G)
	branching_nodes_trait_dict = get_trait_of_nodes(branching_nodes,G)
	# print "branching_nodes : " + str(branching_nodes)
	# print "branching_nodes_trait_dict : " + str(branching_nodes_trait_dict)
	patterns_dict = {
		"for-seq-for": 0,
		"for-seq-if": 0,
		"for-nested-if": 0,
		"for-nested-for": 0,
		"if-seq-if": 0,
		"if-seq-for": 0,
		"if-nested-for": 0,
		"if-nested-if": 0,
	}
	for a,b in itertools.combinations(branching_nodes_trait_dict.keys(),2):
		# print "itertoosl : " + a + "," + b
		label_a = cfg_nodes_dictionary[a]["label"]
		match_obj_a = re.search(r'%(.*):', label_a)
		instruction_a = re.findall(r'\d+',match_obj_a.group())
		label_b = cfg_nodes_dictionary[b]["label"]
		match_obj_b = re.search(r'%(.*):', label_b)
		instruction_b = re.findall(r'\d+',match_obj_b.group())

		if int(instruction_a[0]) < int(instruction_b[0]):
			node_1 = a
			node_2 = b
			
		else:
			node_1 = b
			node_2 = a
			
		relationship_index = check_relationship(node_1,node_2,start_node,end_node,G)
		# print relationship_index
		if relationship_index == 1:
			pattern = branching_nodes_trait_dict[node_1] + "-seq-" + branching_nodes_trait_dict[node_2]
			if pattern in patterns_dict:
				patterns_dict[pattern] += 1
			else:
				patterns_dict[pattern] = 1
		if relationship_index == 0:
			paths_2_nodes = list(nx.all_simple_paths(G,source=node_1,target=node_2))
			is_1_layer_nested = True
			for path in paths_2_nodes:
				for branching_node in branching_nodes:
					if branching_node in path and branching_node != node_1:
						if check_relationship(branching_node,node_2,start_node,end_node,G) == 0:
							is_1_layer_nested = False
							break;
			if is_1_layer_nested == True:
				pattern = branching_nodes_trait_dict[node_1] + "-nested-" + branching_nodes_trait_dict[node_2]
				if pattern in patterns_dict:
					patterns_dict[pattern] += 1
				else:
					patterns_dict[pattern] = 1
	return patterns_dict
				


# with open(CFG_PATH,"r") as f:
# 	data = f.read()

# G = nx_agraph.from_agraph(pygraphviz.AGraph(data))


# # print G.nodes()

# s = get_sorted_nodes_dictionary(G)

# for k,v in s.iteritems():
# 	print k
# cfg_nodes_dictionary = conduct_nodes_dictionary(G)
# start_node,end_node = get_start_and_end_node(cfg_nodes_dictionary)
# branching_nodes = get_branching_nodes(G)
# branching_nodes_trait_dict = get_trait_of_nodes(branching_nodes,G)
# graphs = list(nx.connected_component_subgraphs(G))



# print end_node
# print check_relationship("Node0x557c8f01ef60","Node0x557c8f01e9f0",start_node,end_node,G)
# print extract_patterns(G)
# print extract_patterns2(G)
