import os
import networkx as nx
import matplotlib.pyplot as plt
import networkx.drawing.nx_pydot as nx_pydot
from collections import defaultdict
import re
import pygraphviz
import openpyxl
import collections
# import xlsd
from networkx.drawing import nx_agraph
import util
import pydot
import copy

# wb = openpyxl.load_workbook('features.xlsx')
wb = openpyxl.load_workbook('../features_new.xlsx')
worksheet = wb.get_sheet_by_name('Sheet1')
all_features = list()
for row in worksheet.iter_rows('A1:UK'.format(worksheet.min_row,worksheet.max_row)):
    for cell in row:
    	if cell.value:
    		all_features.append(cell.value)
standard_features_dict = dict.fromkeys(all_features)
for key,value in standard_features_dict.iteritems():
	standard_features_dict[key] = 0

for root,dirs,files in os.walk("/home/quocnghi/courses/software_mining/extraction/DotData"):
	
	
	index = 0
	for file in files:
		path = os.path.join(root,file)
		print path

		split = path.split("/")
		index = split[7] + "_" + split[8].split(".")[0]
		print index
		label = split[7]
		# with open(path, "r") as ins:
		# 	data=ins.read()
		try:
			pydot_graphs = pydot.graph_from_dot_file(path)
			# G=nx_agraph.from_agraph(pygraphviz.AGraph(data))
			all_merge_dict = copy.deepcopy(standard_features_dict)
			for graph in pydot_graphs:
				G = nx_pydot.from_pydot(graph)
				# G=nx.MultiDiGraph(nx_pydot.read_dot("./DotData/1/1162.dot"))
				# print(list(G.nodes()))
				if len(G.nodes()) > 2:
					G = nx.convert_node_labels_to_integers(G)
					nodes = list(G.nodes())
					in_edges = G.in_edges()
					out_edges = G.out_edges()

					print nodes
					
					merge = {}
					in_dict = {}
					out_dict = {}

					print "#######################"
					for node in nodes:
						for in_edge in in_edges:
							if in_edge[1] == node:
								if node in in_dict:
									in_dict[node] +=1
								else:
									in_dict[node] = 1

						for out_edge in out_edges:
							if out_edge[0] == node:
								if node in out_dict:
									out_dict[node] +=1
								else:
									out_dict[node] = 1
				
					keys_in = set(in_dict.keys())
					keys_out = set(out_dict.keys())
					intersection = keys_in | keys_out
					

					print "#######################"

					pq_nodes_dict = util.get_pq_nodes_dictionary(intersection,out_dict,in_dict)


					# print pq_nodes_dict

					for feature in all_features:
						if feature not in pq_nodes_dict:
							pq_nodes_dict[feature] = 0

					print "#######################"
					paths_dict = {}
					for i in range(len(nodes)):
					    for j in range(i + 1, len(nodes)):
					        for path in nx.all_simple_paths(G, source=nodes[i], target=nodes[j]):
					        	path_length = len(path)
					        	if path_length in paths_dict:
					        		paths_dict[str(path_length) + "-path"] += 1
					        	else:
					        		paths_dict[str(path_length) + "-path"] = 1

					print paths_dict

					print "#######################"
					# merge = copy.deepcopy(pq_nodes_dict)
					merge.update(pq_nodes_dict)
					merge.update(paths_dict)
					print merge

					degree_dict = util.get_degree_dictionary(G)

					degree_features_dict = util.get_degree_features_dictionary(degree_dict)

					print degree_features_dict


					print "#######################"
					print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					in_nodes_dict = util.get_in_nodes_dictionary(G)
					out_nodes_dict = util.get_out_nodes_dictionary(G)

					merge.update(degree_features_dict)
					merge.update(in_nodes_dict)
					merge.update(out_nodes_dict)

					for key,value in merge.iteritems():
						if key in all_merge_dict: 
							all_merge_dict[key] += value
						else:
							all_merge_dict[key] = value


			print "final features set : " + str(len(all_merge_dict))
			sorted_merge = collections.OrderedDict(sorted(all_merge_dict.items()))

			print sorted_merge.values()

			for k in sorted_merge.keys():
				if k not in all_features:
					del sorted_merge[k]
			# if len(sorted_merge) == 557:
			line = ",".join(str(v) for v in sorted_merge.values())
			line = line + "," + str(label)
			write_line = index + "," + line
			with open("../features/final_features_with_sample_id_full_multiple_graphs3.txt", "a") as f:
				
				print write_line
				f.write(write_line + "\n")
		except :
			continue


		
